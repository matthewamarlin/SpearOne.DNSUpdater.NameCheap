﻿param($Version = "0.0.0-dev", $BuildConfiguration = "Release")

$ScriptsFolder = "$PSScriptRoot\..\..\Scripts"
$BuildFolder = "$PSScriptRoot\..\..\..\Build"
$OutputFolder = "$BuildFolder\$BuildConfiguration"
$SourceFolder = "$PSScriptRoot\..\..\..\Source"

Push-Location
Set-Location $SourceFolder

Remove-Item -Path $BuildFolder -Force -Recurse -ErrorAction Ignore
New-Item -ItemType Directory $OutputFolder -Force

Set-Location $SourceFolder

nuget restore
dotnet msbuild ".\SpearOne.DNSUpdater.NameCheap.sln" "/p:Configuration=Release" "/p:OutputPath=$OutputFolder"

Set-Location $OutputFolder

Move-Item -Path ".\appsettings.release.json" -Destination ".\appsettings.json" -Force
Copy-Item -Path "$ScriptsFolder\Prod\*" -Destination ".\"

Compress-Archive -Path ".\*" -DestinationPath "..\SpearOne.DNSUpdater.NameCheap.$Version.zip" -CompressionLevel Optimal -Force

Pop-Location