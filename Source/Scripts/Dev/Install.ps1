﻿Push-Location
Set-Location "$PSScriptRoot\..\..\SpearOne.DNSUpdater.NameCheap.Hosts.WindowsService\bin\Release\"

Set-Alias installutil "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\installutil.exe"

installutil "SpearOne.DNSUpdater.NameCheap.Hosts.WindowsService.exe"
net start "SpearOne.DNSUpdater.NameCheap.Hosts.WindowsService"

Pop-Location