﻿Push-Location
Set-Location $PSScriptRoot

Set-Alias installutil "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\installutil.exe"

net stop "SpearOne.DNSUpdater.NameCheap.Hosts.WindowsService"
installutil /u "SpearOne.DNSUpdater.NameCheap.Hosts.WindowsService.exe"

Pop-Location