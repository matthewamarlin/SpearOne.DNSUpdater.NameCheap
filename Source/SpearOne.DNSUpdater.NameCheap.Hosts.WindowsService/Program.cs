﻿using log4net;
using System;

namespace SpearOne.DNSUpdater.NameCheap.Hosts.WindowsService
{
    public class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program)); 

        public static void Main(string[] args)
        {
#if DEBUG
            new ServiceInitializer().StartConsoleHost();
#else
            if (!Environment.UserInteractive)
            {
                Log.Debug("Environment is not user interactive starting service host.");
                new ServiceInitializer().StartServiceHost();
            }
            else
            {
                Log.Debug("Environment is user interactive starting console shell.");
                new ServiceInitializer().StartConsoleHost();
            }
#endif
        }
    }
}
