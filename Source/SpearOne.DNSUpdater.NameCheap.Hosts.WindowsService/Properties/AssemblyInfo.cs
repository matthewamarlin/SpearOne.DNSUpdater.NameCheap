﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DNSUpdater.NameCheap.Hosts.WindowsService")]
[assembly: AssemblyDescription("The Windows service version of the NameCheap DNS updater.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("2F315D7B-B5A2-4FF5-8396-8E323E4B7C62")]

[assembly: AssemblyCompany("SpearOne")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("Copyright © SpearOne 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyInformationalVersion("1.0.0")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config", Watch = true)]