﻿using Microsoft.Extensions.Configuration;
using SpearOne.DNSUpdater.NameCheap.Logic;
using System;
using System.ServiceProcess;

namespace SpearOne.DNSUpdater.NameCheap.Hosts.WindowsService
{
    partial class ServiceHost : ServiceBase
    {
        public static NameCheapDNSUpdater Service;

        public ServiceHost()
        {
            InitializeComponent();            
        }

        #region Overrides of ServiceBase

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            StartService(args);
        }

        protected override void OnStop()
        {           
            StopService();
            base.OnStop();
        }

        #endregion

        public static void StartService(string[] args = null)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();
            Service = new NameCheapDNSUpdater(configuration);

            Service.Start();
        }

        private static void StopService()
        {
            Service.Stop();
        }

        public static void RestartService()
        {
            Service.Stop();
            Service.Start();
        }
    }
}
