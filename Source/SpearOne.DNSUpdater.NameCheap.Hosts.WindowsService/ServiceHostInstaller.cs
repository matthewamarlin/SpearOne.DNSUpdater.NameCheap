﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace SpearOne.DNSUpdater.NameCheap.Hosts.WindowsService
{
    [RunInstaller(true)]
    public partial class ServiceHostInstaller : Installer
    {
        public ServiceHostInstaller()
        {
            //InitializeComponent();
            Install();
        }

        public void Install()
        {
            Installers.Add(new ServiceInstaller
            {
                ServiceName = "SpearOne.DNSUpdater.NameCheap.Hosts.WindowsService",
                DisplayName = "SpearOne - NameCheap DNS Updater",
                Description = "Hosts the DNS NameCheap DNS Updater",
                StartType = ServiceStartMode.Automatic
            });

            Installers.Add(new ServiceProcessInstaller { Account = ServiceAccount.LocalSystem });
        }
    }
}
