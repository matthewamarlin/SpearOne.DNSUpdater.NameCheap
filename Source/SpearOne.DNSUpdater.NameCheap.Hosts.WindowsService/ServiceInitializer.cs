﻿using log4net;
using log4net.Core;
using System;
using System.ServiceProcess;
using System.Threading;

namespace SpearOne.DNSUpdater.NameCheap.Hosts.WindowsService
{
    public class ServiceInitializer
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ServiceInitializer)); 

        public ServiceInitializer()
        { }

        public void StartConsoleHost()
        {
            try
            {
                ServiceHost.StartService();
            }
            catch (Exception ex)
            {
                LogException(ex);
            }

            Thread.Sleep(Timeout.Infinite);
        }

        public void StartServiceHost()
        {
            try
            {
                var servicesToRun = new ServiceBase[] { new ServiceHost(), };
                ServiceBase.Run(servicesToRun);
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
        }

        private void LogException(Exception ex)
        {
            Log.FatalFormat("An error occured during the application startup: \n{0}\n{1}", ex.Message, ex.InnerException);
        }
    }
}
