﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Web;
using System.Xml.Serialization;
using Quartz;
using Quartz.Impl;
using System.Threading.Tasks;
using log4net;

namespace SpearOne.DNSUpdater.NameCheap.Logic
{
    public class DNSUpdateJob : IJob
    {
        private readonly HttpClient _httpClient = new HttpClient();
        private readonly IConfigurationRoot _configuration;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DNSUpdateJob)); // Consider instance ID??

        public DNSUpdateJob(IConfigurationRoot configuration)
        {
            Log.Debug("DNS Update JOB Constructed through default Normal!");
            _configuration = configuration;
        }

        public DNSUpdateJob()
        {
            Log.Debug("DNS Update JOB Constructed through default constructor???");
        }

        public UpdateResponse UpdateIP(string index)
        {
            try
            {
                var builder = new UriBuilder("https://dynamicdns.park-your-domain.com/update")
                {
                    Port = -1
                };
                var query = HttpUtility.ParseQueryString(builder.Query);

                var profile = _configuration.GetSection($"profiles:{index}").Get<ProfileConfig>();

                Log.Debug($"Executing DNS Update job for profile {index}:");
                Log.Debug(profile);

                query["ip"] = profile.Ip == "dynamic" ? PublicIPAddress() : profile.Ip;
                query["by"] = profile.By;
                query["host"] = profile.Host;
                query["domain"] = profile.Domain;
                query["password"] = profile.Password;

                builder.Query = query.ToString();
                var url = builder.ToString();

                Console.WriteLine(url);

                // TODO Make function async ??
                var result = _httpClient.GetAsync(url).Result;
                var responseContent = result.Content.ReadAsStringAsync().Result;

                XmlSerializer serializer = new XmlSerializer(typeof(UpdateResponse));
                using (TextReader reader = new StringReader(responseContent))
                {
                    var updateResponse = (UpdateResponse)serializer.Deserialize(reader);

                    Console.WriteLine(JsonConvert.SerializeObject(updateResponse));

                    return updateResponse;
                }
            }
            catch (Exception ex)
            {
                Log.FatalFormat("An error occured during the update job: \n{0}\n{1}", ex.Message, ex.InnerException);
            }

            return null;
        }

        public static string PublicIPAddress()
        {
            string uri = "http://checkip.dyndns.org/";
            string ip = string.Empty;

            using (var client = new HttpClient())
            {
                var result = client.GetAsync(uri).Result.Content.ReadAsStringAsync().Result;

                ip = result.Split(':')[1].Split('<')[0];
            }

            return ip;
        }

        public Task Execute(IJobExecutionContext context)
        {
            var profileNumber = context.MergedJobDataMap.Get("profile").ToString();

            // TODO Consider cancellationToken
            return Task.Run(() => this.UpdateIP(profileNumber));
        }
    }
}
