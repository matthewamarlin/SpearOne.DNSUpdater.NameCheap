﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz.Spi;
using System.Reflection;
using log4net;
using log4net.Config;
using System.IO;
using System.Threading;

namespace SpearOne.DNSUpdater.NameCheap.Logic
{
    public class NameCheapDNSUpdater
    {
        private readonly ServiceProvider container;
        private readonly QuartzStartup sheduler;
        private readonly ILog _log;

        public NameCheapDNSUpdater(IConfigurationRoot configuration) {
            
            // Configure Logging
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            _log = LogManager.GetLogger(typeof(NameCheapDNSUpdater));
            _log.Info("Configuring DI Container.");

            var services = new ServiceCollection();

            services.AddTransient(_ => { return configuration; });
            services.AddTransient<IJobFactory, JobFactory>();
            services.AddTransient<QuartzStartup>();
            services.AddTransient<DNSUpdateJob>();

            this.container = services.BuildServiceProvider();
            this.sheduler = container.GetService<QuartzStartup>();
        }

        public void Start()
        {
            _log.Info("Starting Task Sheduler.");
            this.sheduler.Start();
        }

        public void Stop()
        {
            _log.Info("Stopping Task Sheduler.");
            this.sheduler.Stop();
        }
    }
}
