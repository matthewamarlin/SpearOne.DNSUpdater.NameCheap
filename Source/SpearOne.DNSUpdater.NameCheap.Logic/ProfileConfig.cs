﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace SpearOne.DNSUpdater.NameCheap.Logic
{
    public class ProfileConfig
    {
        public string By { get; set; }
        public string Domain { get; set; }
        public string Host { get; set; }
        public string Ip { get; set; }
        public string Password { get; set; }
        public string Cron { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
