﻿using System;
using System.Collections.Specialized;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using log4net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Binder;
using System.Configuration;
using System.Linq;

namespace SpearOne.DNSUpdater.NameCheap.Logic
{
    public class QuartzStartup
    {
        private IScheduler _scheduler; // after Start, and until shutdown completes, references the scheduler object
        private readonly IJobFactory _jobFactory;
        private readonly IConfigurationRoot _configuration;
        private static readonly ILog Log = LogManager.GetLogger(typeof(QuartzStartup));

        public QuartzStartup(IJobFactory jobFactory, IConfigurationRoot configuration)
        {
            _jobFactory = jobFactory;
            _configuration = configuration;
        }

        // starts the scheduler, defines the jobs and the triggers
        public void Start()
        {
            Log.Debug("Initialisation Quartz TaskSheduler");

            if (_scheduler != null)
            {
                throw new InvalidOperationException("Already started.");
            }

            var properties = new NameValueCollection
            {
                // json serialization is the one supported under .NET Core (binary isn't)
                ["quartz.serializer.type"] = "json",

                // the following setup of job store is just for example and it didn't change from v2
                //["quartz.jobStore.type"] = "Quartz.Impl.AdoJobStore.JobStoreTX, Quartz",
                //["quartz.jobStore.useProperties"] = "false",
                //["quartz.jobStore.dataSource"] = "default",
                //["quartz.jobStore.tablePrefix"] = "QRTZ_",
                //["quartz.jobStore.driverDelegateType"] = "Quartz.Impl.AdoJobStore.SqlServerDelegate, Quartz",
                //["quartz.dataSource.default.provider"] = "SqlServer-41", // SqlServer-41 is the new provider for .NET Core
                //["quartz.dataSource.default.connectionString"] = @"Server=(localdb)\MSSQLLocalDB;Database=Quartz;Integrated Security=true"
            };

            _scheduler = new StdSchedulerFactory(properties).GetScheduler().Result;
            _scheduler.JobFactory = _jobFactory;
            _scheduler.Start().Wait();

            var profiles = _configuration.GetSection("profiles").GetChildren();

            Log.Debug($"Registering Jobs for {profiles.Count()} profiles.");

            foreach (var item in profiles.Select((configObject, index) => new { index, profile = configObject.Get<ProfileConfig>() }))
            {
                Log.Debug($"Processing Profile: {item.index}");
                Log.Debug(item.profile);

                var dnsUpdateJob = JobBuilder.Create<DNSUpdateJob>()
                .WithIdentity($"DnsUpdateJob_{item.index}")
                .Build();

                var cron = item.profile.Cron;

                var dnsUpdateTrigger = TriggerBuilder.Create()
                    .WithIdentity($"DnsUpdateCron_{item.index}")
                    .UsingJobData("profile", item.index) // TODO Pass data of profiles instead of reference index as the persisatance store wont work properly this way
                    .StartNow()
                    .WithCronSchedule(cron)
                    .Build();

                _scheduler.ScheduleJob(dnsUpdateJob, dnsUpdateTrigger).Wait();
            }

            Log.Debug("Completed Quartz TaskSheduler Initialisation");
        }

        // initiates shutdown of the scheduler, and waits until jobs exit gracefully (within allotted timeout)
        public void Stop()
        {
            if (_scheduler == null)
            {
                return;
            }

            // give running jobs 30 sec (for example) to stop gracefully
            if (_scheduler.Shutdown(waitForJobsToComplete: true).Wait(30000))
            {
                _scheduler = null;
            }
            else
            {
                // jobs didn't exit in timely fashion - log a warning...
            }
        }
    }
}
