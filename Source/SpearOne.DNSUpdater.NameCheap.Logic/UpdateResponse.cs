﻿using System.Xml.Serialization;

namespace SpearOne.DNSUpdater.NameCheap.Logic
{
    [XmlRoot("interface-response")]
    public class UpdateResponse
    {
        [XmlElement("Command")]
        public string Command { get; set; }

        [XmlElement("Language")]
        public string Language { get; set; }

        [XmlElement("IP")]
        public string IP { get; set; }

        [XmlElement("ErrCount")]
        public int ErrCount { get; set; }

        [XmlElement("ResponseCount")]
        public int ResponseCount { get; set; }

        [XmlElement("Done")]
        public bool Done { get; set; }
    }
}
